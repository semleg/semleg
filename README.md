# SEMLEG

In different domains, compliance with legal documents about industrial maintenance is crucial. Legal industrial maintenance is the legal commitment of a company to control, maintain and repair its equipments. With the evolution of legal texts, companies increasingly adopting automatic processing of legal texts in order to extract their key elements and to support the task of analysis and compliance. To perform this information extraction, a number of state-of-the-art proposal relies on a semantic model. Based on existing models in the legislative domain but also in the industrial maintenance domain, we propose a new semantic model for the legal industrial maintenance: SEMLEG (SEmantic Model for the LEGal maintenance). This model results from an analysis of documents extracted from the French governmental website: Légifrance.

## Current version

V1.0.0
